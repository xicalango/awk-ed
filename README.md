# awk-ed

minimalistic ed implementation in awk

Currently supported:

- print (p)
- print with line number (n)
- insert (i)
- append (a)
- write buffer to file (w)
- write buffer to file with filename (w <filename>)
- deleting lines (d)
- range addressing (i,j); i, j can be:
    - line numbers
    - `.` for current line
    - `$` for the last line
    - `+` for next line
    - `-` for previous line
    - speical character `%` will be replaced with `1,$`
- quit (q)
- basic error reporting (?)
