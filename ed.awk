#!/usr/bin/awk -f

# globals
# buffer: line buffer
# buffer_end: number of last line in buffer
# filename: filename of current file
# cur: current line
# user_line: raw line read from user
# running: when set to 1 execute mainloop
# mode: current mode
# error: if 0: no error, if negative: error according to error codes (TODO)
# dirty: if 0: buffer contents == file contents; if 1: buffer buffer contents != file contents
# ibuffer: insert buffer
# icur: current line in insert buffer


## traditional ed error reporting
## resets `error`
## @param error (in/out) last error
function print_error() {
  print "?"
  error = 0
}

## init buffer, optionaly with the contents of a file
## in case a file was loaded, print number of lines
## @param buffer      (out) initialized buffer
## @param filename    (out) filename of current file
## @param cur         (out) last line in file
function load_file() {
  filename = ARGV[1]

  if (length(filename) == 0) {
      buffer[0] = ""
      buffer_end = -1
      cur = 0
  } else {
    i = 0
    while (getline) {
      buffer[i] = $0
      i++
    }
    print i
    buffer_end = i - 1
    cur = i - 1
  }

}

## parse the commands from user_line
## @param user_line (in)  line passed in by user
## @param cmd       (out) parsed user command
## @param start     (out) start of selected range
## @param end       (out) end of selected range
function parse_cmd() {

  # cmd starts at the first non-numeric character
  pos_cmd = match(user_line, "[a-z]")

  if (pos_cmd == 0) {
    # no command? then the full line is a range description
    cmd = ""
    range_str = user_line
  } else {
    cmd = tolower(substr(user_line, pos_cmd))
    range_str = substr(user_line, 1, pos_cmd - 1)
  }

  n = split(range_str, range_a, ",")

  if (range_str == "%") {
    start = 1
    end = "$"
  } else if (length(range_str) == 0) {
    start = "."
    end = "."
  } else if (n == 0 || n == 1) {
    start = range_str
    end = range_str
  } else if (n == 2) {
    start = range_a[1]
    end = range_a[2]
  }

  return 0
}

## evaluate location string
## . = current line
## $ = end of file
## + = next line
## - = previous line
## @param loc location string to resolve
## @return actual location in buffer; -1 on error
function eval_location(loc) {
  if (loc == ".") {
    loc = cur
  } else if (loc == "$") {
    loc = buffer_end
  } else if (loc == "+") {
    loc = cur + 1
  } else if (loc == "-") {
    loc = cur - 1
  } else {
    loc--
  }

  if (loc < 0 || loc > buffer_end) {
    return -1
  }
  return loc
}

## delete lines from buffer
## @param dirty      (out)    after deletion buffer is dirty
## @param buffer     (in/out) deletes lines from buffer
## @param buffer_end (in/out) updates buffer_end to reflect new end of buffer
function delete_lines() {
  dirty = 1
  j = start
  for (i = end + 1; i <= buffer_end; i++) {
    buffer[j] = buffer[i]
    j++
  }

  buffer_end = buffer_end - (end - start + 1)
}

## write contents of buffer to file
## @param dirty       (out)     on successful writing operation, buffer is set as not dirty
## @param cmd         (in)      cmd is evaluated for optional filename
## @param buffer      (in)      buffer is read to be written
## @param buffer_end  (in)      buffer is only read until buffer_end (incl)
## @param filename    (in/out)  filename to write into; can be updated through parameter to "w" command
function write_file() {
  n = match(cmd, " ")
  if (n != 0) {
    filename = substr(cmd, n + 1)
  }

  if (length(filename) == 0) {
    return -1
  }

  if (buffer_end == -1) {
    print "" > filename
  } else {
    for (i = 0; i <= buffer_end; i++) {
      print buffer[i] > filename
    }
  }

  dirty = 0
  return 0
}

## try to quit ed
## @param dirty   (in/out)  if contents of buffer is dirty, reset dirty flag and warn user
## @param running (out)     set to 0 when quitting ed
function quit() {
  if (dirty) {
    dirty = 0
    return -1
  } else {
    running = 0
    return 0 
  }
}

## switch to insert mode
## @param cmd         (in)  user command is read to determine insert position 
## @param end         (in)  used to determine insert position
## @param insert_pos  (out) set to position to insert new lines at
## @param mode        (out) set to MODE_INSERT
## @param icur        (out) insert cursor is set to 0
function switch_insert() {
  if (cmd == "i") {
    insert_pos = end
  } else if (cmd == "a") {
    insert_pos = end + 1
  }
  mode = MODE_INSERT
  icur = 0

  return 0
}

## end insert mode by inserting ibuffer into working buffer
## @param buffer      (in/out)  buffer is being updated with content of ibuffer
## @param buffer_end  (in/out)  buffer_end is updated to reflect new last line in buffer
## @param ibuffer     (in)      buffer containing new lines
## @param icur        (in)      cursor pointing to line after last in ibuffer
## @param cur         (out)     cur is set to the last inserted line
## @param insert_pos  (in)      position where to insert in the working buffer
## @param dirty       (out)     buffer is dirty after inserting
## @param mode        (out)     mode is set to MODE_CMD
function finalize_insert() {
  j = 0
  for(i = 0; i < insert_pos; i++) {
    tmpbuf[j] = buffer[i]
    j++
  }
  for(i = 0; i < icur; i++) {
    tmpbuf[j] = ibuffer[i]
    j++
  }
  for(i = insert_pos; i <= buffer_end; i++) {
    tmpbuf[j] = buffer[i]
    j++
  }

  buffer_end = buffer_end + icur

  for(i = 0; i <= buffer_end; i++) {
    buffer[i] = tmpbuf[i]
  }

  delete tmpbuf

  dirty = 1
  mode = MODE_CMD
  cur = cur + icur
  return 0
}

## evaluate command
## @param cmd         (in)      user provided command
## @param start       (in/out)  start of range; will be replaced with actual line in buffer
## @param end         (in/out)  end of range; will be replaced with actual line in buffer
## @param cur         (in/out)  used to evaluate range; will be updated to end
## @param buffer      (in/out)  working buffer to operate on
function eval_cmd() {

  # evaluate locations

  start = eval_location(start)
  end = eval_location(end)

  # location agnostic commands

  cmd1 = substr(cmd, 1, 1)

  if (cmd1 == "q") {
    return quit()
  } else if (cmd1 == "w") {
    return write_file()
  } else if (cmd1 == "i" || cmd1 == "a") {
    return switch_insert()
  }

  # validate locations

  if (start == -1 || end == -1 || start > end) {
    return -1
  }

  cur = end

  if (length(cmd) == 0) {
    print buffer[cur]
  } else if (cmd1 == "p") {
    for (i = start; i <= end; i++) {
      print buffer[i]
    }
  } else if (cmd1 == "n") {
    for (i = start; i <= end; i++) {
      print i+1, buffer[i]
    }
  } else if (cmd1 == "d") {
    delete_lines()
  }

  return 0
}

## entry point
BEGIN {

  MODE_CMD = 0
  MODE_INSERT = 1

  load_file()

  running = 1
  mode = MODE_CMD

  while (running) {
    status = getline user_line < "/dev/stdin"
    if (status == 0) {
      print "EOF received. Exiting..."
      exit
    }
  
    if (mode == MODE_CMD) {
      status = parse_cmd()
      if (status == -1) {
        print_error()
        continue
      }

      status = eval_cmd()
      if (status == -1) {
        print_error()
        continue
      }
    } else if (mode == MODE_INSERT) {
      if (user_line == ".") {
        status = finalize_insert()
        if (status == -1) {
          print_error()
          continue
        }
      } else {
        ibuffer[icur] = user_line
        icur++
      }
    }
  }
}

